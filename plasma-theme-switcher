#!/bin/bash

exec_path="$0"
operation="$1"
variant="$2"
time="$3"

# How to use this script:
#   1. Create a variant
#   2. Activate the variant manually or create a timer for it.

# To create a variant:
#   1. Add the variant name to the list "valid_variants" below.
#   2. Implement the variant's behavior in the switch case statement below. (In
#       function "apply".)

# To activate a variant, run 'plasma-theme-switcher apply <variant>'.
# To create a timer for a variant, run
#   'plasma-theme-switcher create-timer <variant> <HH:MM>', where
#    <HH:MM> is the time to switch to <variant> in 24-hour time.


# The LIST OF VALID VARIANTS
export valid_variants="morning afternoon evening night"

function apply {
    variant=$1

    if [ -z "$variant" ]
    then
        echo "ERROR! Must specify a variant!"
        exit 2
    fi

    # IMPLEMENT VARIANT BEHAVIOR HERE!
    #
    #   To change global theme, use 'plasma-apply-lookandfeel --apply=<global-theme>'.
    #       To see the options use 'plasma-apply-lookandfeel --list'.
    #
    #   To change the color scheme, use 'plasma-apply-colorscheme <scheme>'.
    #       To see the options use 'plasma-apply-colorscheme --list-schemes'.
    #   You can also change the accent color with 'plasma-apply-colorscheme --accent-color=<code>'.
    #       Where <code> comes from https://www.w3.org/TR/SVG11/types.html#ColorKeywords. Includes hex format.
    #
    #   To change the wallpaper, use 'plasma-apply-wallpaperimage "<path to image>"'.
    #
    case $variant in
        morning)
            plasma-apply-lookandfeel --apply=org.kde.breeze.desktop
            plasma-apply-colorscheme 149024-GoldenHoneyOak
            plasma-apply-wallpaperimage "/home/josh/Pictures/Wallpapers/OSX Mavericks Desktop Pictures/Solid Colors/Solid Kelp.png"
            notify-send --icon=brightness-high --app-name=plasma-theme-switcher.sh "Switched to morning theme" "Marnin' to ya\!"
            ;;
        afternoon)
            plasma-apply-lookandfeel --apply=org.kde.breeze.desktop
            plasma-apply-wallpaperimage "/home/josh/Pictures/Wallpapers/OSX Mavericks Desktop Pictures/Solid Colors/Solid Aqua Blue.png"
            notify-send --icon=preferences-desktop-theme-global --app-name=plasma-theme-switcher.sh "Switched to afternoon theme" "It's afternoon\!"
            ;;
        evening)
            plasma-apply-lookandfeel --apply=org.kde.breezetwilight.desktop
            plasma-apply-colorscheme calm-eyes
            plasma-apply-wallpaperimage "/home/josh/Pictures/Wallpapers/OSX Mavericks Desktop Pictures/Solid Colors/Solid Aqua Dark Blue.png"
            notify-send --icon=preferences-desktop-theme-global --app-name=plasma-theme-switcher.sh "Switched to evening theme" "Nearing the end of the day it is\!"
            ;;
        night)
            plasma-apply-lookandfeel --apply=org.kde.breezedark.desktop
            plasma-apply-wallpaperimage "/home/josh/Pictures/Wallpapers/OSX Mavericks Desktop Pictures/Solid Colors/Solid Gray Dark.png"
            notify-send --icon=weather-clear-night --app-name=plasma-theme-switcher.sh "Switched to night theme" "'Gnight\!"
            ;;
        *)
            echo "ERROR! Invalid variant \"$variant\"!"
            notify-send --icon=dialog-error --app-name=plasma-theme-switcher.sh "Error! Invalid variant!" "The plasma-theme-switcher.sh script was called with a variant that wasn't in the list: \"$variant\"\!"
            exit 3
            ;;
    esac
}

function create-timer {
    exec_path=$1
    variant=$2
    time=$3

    if [ -z "$variant" ]
    then
        echo "ERROR! Must specify a variant!"
        exit 2
    elif [ -z "$time" ]
    then
        echo "ERROR! Must specify a time to activate!"
        exit 4
    fi

    SYSTEMD_USER=$HOME/.config/systemd/user
    [ -d $SYSTEMD_USER ] || mkdir -p $SYSTEMD_USER

    echo "[Unit]
Description=Switch desktop to $variant theme.

[Service]
ExecStart=$exec_path apply $variant

[Install]
WantedBy=default.target
" | tee $SYSTEMD_USER/theme-switch-$variant.service

    echo "[Unit]
Description=Switch desktop to $variant theme.

[Timer]
OnCalendar=*-*-* $time
Persistent=true

[Install]
WantedBy=timers.target
" | tee $SYSTEMD_USER/theme-switch-$variant.timer

    systemctl --user enable --now theme-switch-$variant.timer
    systemctl --user status theme-switch-$variant.timer

}

function remove-timer {
    variant=$1

    SYSTEMD_USER=$HOME/.config/systemd/user

    systemctl --user disable --now theme-switch-$variant.timer
    systemctl --user status theme-switch-$variant.timer

    rm --verbose $SYSTEMD_USER/theme-switch-$variant.{service,timer}
}

# Check validity of variant
function valid-variant {
    variant=$1

    valid_variant=false
    for candidate in $valid_variants
    do
        [ "$variant" == "$candidate" ] && valid_variant=true
    done
    if [ "$valid_variant" != true ]
    then
        echo "ERROR! Invalid variant \"$variant\"!"
        exit 3
    fi
}

function print-help {
    echo
    echo "Plasma Theme Switcher"
    echo "  A simple utility to manage plasma theme switching."
    echo
    echo "Usage: plasma-theme-switcher <operation> <variant> <time>"
    echo
    echo "  Where <operation> is one of:"
    echo "      - list-variants - list out the valid variants implemented in this script."
    echo "      - apply         - apply the given variant."
    echo "      - create-timer  - create a user systemd timer to switch to a variant at a given time."
    echo "      - remove-timer  - remove a previously created timer."
    echo "      - help          - print this help menu."
    echo
    echo "To view active timers run 'systemctl --user list-timers'."

    exit 0
}

[ $# -lt 1 ] && print-help

# Run operation
case $operation in

    list-variants)
        for item in $valid_variants
        do
            echo "$item"
        done
        ;;

    apply)
        valid-variant $variant
        apply $variant
        ;;

    create-timer)
        valid-variant $variant
        create-timer $exec_path $variant $time
        ;;

    remove-timer)
        valid-variant $variant
        remove-timer $variant
        ;;

    help)
        print-help
        ;;

    *)
        echo "ERROR! Invalid operation \"$operation\"!"
        echo "  For usage information run 'plasma-theme-switcher help'"
        ;;
esac


